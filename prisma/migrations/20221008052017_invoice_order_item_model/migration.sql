/*
  Warnings:

  - You are about to drop the column `invoice_url` on the `orders` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "orders" DROP COLUMN "invoice_url";

-- CreateTable
CREATE TABLE "invoice_order_item" (
    "order_id" BIGINT NOT NULL,
    "invoice_number" BIGSERIAL NOT NULL,
    "invoice_url" TEXT,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "invoice_order_item_pkey" PRIMARY KEY ("order_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "invoice_order_item_order_id_key" ON "invoice_order_item"("order_id");

-- CreateIndex
CREATE UNIQUE INDEX "invoice_order_item_invoice_url_key" ON "invoice_order_item"("invoice_url");

-- AddForeignKey
ALTER TABLE "invoice_order_item" ADD CONSTRAINT "invoice_order_item_order_id_fkey" FOREIGN KEY ("order_id") REFERENCES "order_item"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
