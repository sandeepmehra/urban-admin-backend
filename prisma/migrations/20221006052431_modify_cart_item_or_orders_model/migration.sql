/*
  Warnings:

  - You are about to drop the column `price_per_unit` on the `cart_item` table. All the data in the column will be lost.
  - You are about to drop the column `seller_commission` on the `cart_item` table. All the data in the column will be lost.
  - You are about to drop the `invoice` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "invoice" DROP CONSTRAINT "invoice_order_id_fkey";

-- AlterTable
ALTER TABLE "cart_item" DROP COLUMN "price_per_unit",
DROP COLUMN "seller_commission";

-- AlterTable
ALTER TABLE "orders" ADD COLUMN     "invoice_url" TEXT,
ADD COLUMN     "payment_bank_txn_id" TEXT,
ADD COLUMN     "payment_bankname" TEXT,
ADD COLUMN     "payment_gateway_name" TEXT,
ADD COLUMN     "payment_mid" TEXT,
ADD COLUMN     "payment_refund_amt" TEXT,
ADD COLUMN     "payment_respcode" TEXT,
ADD COLUMN     "payment_txn_date" TEXT,
ADD COLUMN     "payment_txn_id" TEXT,
ADD COLUMN     "payment_txn_type" TEXT,
ALTER COLUMN "payment_mode" DROP NOT NULL;

-- DropTable
DROP TABLE "invoice";
