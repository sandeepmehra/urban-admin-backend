/*
  Warnings:

  - You are about to drop the column `product_id` on the `cart_item` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "cart_item" DROP CONSTRAINT "cart_item_product_id_fkey";

-- AlterTable
ALTER TABLE "cart_item" DROP COLUMN "product_id";
