-- CreateTable
CREATE TABLE "user_seller" (
    "id" BIGSERIAL NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "mobile_number" BIGINT NOT NULL,
    "commission_percentage" DOUBLE PRECISION NOT NULL,
    "commission_added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "suspended" BOOLEAN NOT NULL,
    "created_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "user_seller_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_seller_verification" (
    "id" BIGSERIAL NOT NULL,
    "aadhar_card_url" TEXT NOT NULL,
    "pan_card_url" TEXT NOT NULL,
    "selfie_url" TEXT NOT NULL,
    "bank_document_url" TEXT NOT NULL,
    "gst_number" TEXT NOT NULL,
    "live_lat" DOUBLE PRECISION NOT NULL,
    "live_long" DOUBLE PRECISION NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "action_taken" BOOLEAN NOT NULL,
    "verified" BOOLEAN NOT NULL,
    "action_taken_on" TIMESTAMP(3) NOT NULL,
    "seller_id" BIGINT NOT NULL,

    CONSTRAINT "user_seller_verification_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "shop" (
    "id" BIGSERIAL NOT NULL,
    "logo" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "mobile_number" BIGINT NOT NULL,
    "wa_number" BIGINT NOT NULL,
    "url" UUID NOT NULL,
    "rating" DOUBLE PRECISION NOT NULL,
    "seller_id" BIGINT NOT NULL,
    "address_id" BIGINT NOT NULL,

    CONSTRAINT "shop_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product" (
    "id" BIGSERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "product_category" TEXT NOT NULL,
    "shop_id" BIGINT NOT NULL,

    CONSTRAINT "product_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product_variant" (
    "id" BIGSERIAL NOT NULL,
    "price" DOUBLE PRECISION NOT NULL,
    "discounted_price" DOUBLE PRECISION NOT NULL,
    "quantity" INTEGER NOT NULL,
    "color" TEXT NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "approved" BOOLEAN NOT NULL,
    "action_taken" BOOLEAN NOT NULL,
    "action_taken_on" TIMESTAMP(3) NOT NULL,
    "measurement" TEXT NOT NULL,
    "measurement_unit" TEXT NOT NULL,
    "sku" TEXT NOT NULL,
    "country_of_origin" TEXT NOT NULL,
    "hsn_code" TEXT NOT NULL,
    "product_id" BIGINT NOT NULL,

    CONSTRAINT "product_variant_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product_measurement_unit" (
    "id" BIGSERIAL NOT NULL,
    "unit" TEXT NOT NULL,

    CONSTRAINT "product_measurement_unit_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product_variant_seo_keywords" (
    "id" BIGSERIAL NOT NULL,
    "seo_keyword" TEXT NOT NULL,
    "product_variant_id" BIGINT NOT NULL,

    CONSTRAINT "product_variant_seo_keywords_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product_variant_images" (
    "id" BIGSERIAL NOT NULL,
    "url" TEXT NOT NULL,
    "product_variant_id" BIGINT NOT NULL,

    CONSTRAINT "product_variant_images_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product_variant_videos" (
    "id" BIGSERIAL NOT NULL,
    "url" TEXT NOT NULL,
    "product_variant_id" BIGINT NOT NULL,

    CONSTRAINT "product_variant_videos_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "order_item" (
    "id" BIGSERIAL NOT NULL,
    "quantity" INTEGER NOT NULL,
    "seller_commission" DOUBLE PRECISION NOT NULL,
    "price_per_unit" DOUBLE PRECISION NOT NULL,
    "order_id" BIGINT NOT NULL,
    "product_variant_id" BIGINT NOT NULL,
    "seller_id" BIGINT NOT NULL,
    "status" TEXT NOT NULL,
    "dispatch_date" TIMESTAMP(3) NOT NULL,
    "delivery_date" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "order_item_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "address" (
    "id" BIGSERIAL NOT NULL,
    "street" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "pincode" INTEGER NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "address_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "customer_order_address" (
    "id" BIGSERIAL NOT NULL,
    "mobile_number" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "address_id" BIGINT NOT NULL,
    "user_customer_id" BIGINT NOT NULL,

    CONSTRAINT "customer_order_address_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "orders" (
    "id" BIGSERIAL NOT NULL,
    "placed_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "total_price" DOUBLE PRECISION NOT NULL,
    "payment_mode" TEXT NOT NULL,
    "customer_id" BIGINT NOT NULL,
    "customer_order_address_id" BIGINT NOT NULL,

    CONSTRAINT "orders_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "order_item_review_gallary" (
    "id" BIGSERIAL NOT NULL,
    "url" TEXT NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "order_item_id" BIGINT NOT NULL,

    CONSTRAINT "order_item_review_gallary_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "order_item_review" (
    "id" BIGSERIAL NOT NULL,
    "review" TEXT NOT NULL,
    "rating" DOUBLE PRECISION NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "approved" BOOLEAN NOT NULL DEFAULT false,
    "action_taken" BOOLEAN NOT NULL DEFAULT false,
    "action_taken_on" TIMESTAMP(3),
    "order_item_id" BIGINT NOT NULL,

    CONSTRAINT "order_item_review_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "customer_response" (
    "id" BIGSERIAL NOT NULL,
    "response" TEXT NOT NULL,
    "created_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "user_id" BIGINT NOT NULL,
    "complaint_id" BIGINT NOT NULL,

    CONSTRAINT "customer_response_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_customer" (
    "id" BIGSERIAL NOT NULL,
    "name" TEXT NOT NULL DEFAULT E'There',
    "email" TEXT,
    "password" TEXT,
    "mobile_number" TEXT,
    "suspended" BOOLEAN,
    "created_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "address_id" BIGINT,

    CONSTRAINT "user_customer_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_customer_email_otp" (
    "id" BIGSERIAL NOT NULL,
    "email" TEXT NOT NULL,
    "otp" TEXT NOT NULL,
    "generated_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "user_customer_email_otp_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_customer_mobile_otp" (
    "id" BIGSERIAL NOT NULL,
    "mobile_number" TEXT NOT NULL,
    "otp" TEXT NOT NULL,
    "generated_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "user_customer_mobile_otp_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "complaint" (
    "id" BIGSERIAL NOT NULL,
    "subject" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "created_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "open" BOOLEAN NOT NULL DEFAULT true,
    "closed_on" TIMESTAMP(3),
    "customer_id" BIGINT NOT NULL,

    CONSTRAINT "complaint_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "admin_response" (
    "id" BIGSERIAL NOT NULL,
    "response" TEXT NOT NULL,
    "created_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "complaint_id" BIGINT NOT NULL,
    "user_id" BIGINT NOT NULL,

    CONSTRAINT "admin_response_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_admin" (
    "id" BIGSERIAL NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "password" TEXT NOT NULL,

    CONSTRAINT "user_admin_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_seller_default_commission" (
    "id" BIGSERIAL NOT NULL,
    "commission_percentage" DOUBLE PRECISION NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "user_seller_default_commission_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "system_notification" (
    "id" BIGSERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "message" TEXT NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "system_notification_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cart_item" (
    "id" BIGSERIAL NOT NULL,
    "quantity" INTEGER NOT NULL,
    "seller_commission" DOUBLE PRECISION NOT NULL,
    "price_per_unit" DOUBLE PRECISION NOT NULL,
    "seller_id" BIGINT NOT NULL,
    "product_variant_id" BIGINT NOT NULL,
    "customer_id" BIGINT NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_on" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "cart_item_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "categories" (
    "id" BIGSERIAL NOT NULL,
    "category_name" TEXT NOT NULL,
    "category_image_url" TEXT NOT NULL,
    "added_on" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_on" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "categories_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "user_seller_email_key" ON "user_seller"("email");

-- CreateIndex
CREATE UNIQUE INDEX "user_seller_mobile_number_key" ON "user_seller"("mobile_number");

-- CreateIndex
CREATE UNIQUE INDEX "user_seller_verification_seller_id_key" ON "user_seller_verification"("seller_id");

-- CreateIndex
CREATE UNIQUE INDEX "shop_url_key" ON "shop"("url");

-- CreateIndex
CREATE UNIQUE INDEX "shop_seller_id_key" ON "shop"("seller_id");

-- CreateIndex
CREATE UNIQUE INDEX "shop_address_id_key" ON "shop"("address_id");

-- CreateIndex
CREATE UNIQUE INDEX "product_variant_images_url_key" ON "product_variant_images"("url");

-- CreateIndex
CREATE UNIQUE INDEX "product_variant_videos_url_key" ON "product_variant_videos"("url");

-- CreateIndex
CREATE UNIQUE INDEX "customer_order_address_address_id_user_customer_id_key" ON "customer_order_address"("address_id", "user_customer_id");

-- CreateIndex
CREATE UNIQUE INDEX "order_item_review_order_item_id_key" ON "order_item_review"("order_item_id");

-- CreateIndex
CREATE UNIQUE INDEX "user_customer_email_key" ON "user_customer"("email");

-- CreateIndex
CREATE UNIQUE INDEX "user_customer_mobile_number_key" ON "user_customer"("mobile_number");

-- CreateIndex
CREATE UNIQUE INDEX "user_customer_address_id_key" ON "user_customer"("address_id");

-- CreateIndex
CREATE UNIQUE INDEX "user_customer_email_otp_email_otp_key" ON "user_customer_email_otp"("email", "otp");

-- CreateIndex
CREATE UNIQUE INDEX "user_customer_mobile_otp_mobile_number_otp_key" ON "user_customer_mobile_otp"("mobile_number", "otp");

-- CreateIndex
CREATE UNIQUE INDEX "user_admin_email_key" ON "user_admin"("email");

-- CreateIndex
CREATE UNIQUE INDEX "cart_item_product_variant_id_customer_id_key" ON "cart_item"("product_variant_id", "customer_id");

-- AddForeignKey
ALTER TABLE "user_seller_verification" ADD CONSTRAINT "user_seller_verification_seller_id_fkey" FOREIGN KEY ("seller_id") REFERENCES "user_seller"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "shop" ADD CONSTRAINT "shop_seller_id_fkey" FOREIGN KEY ("seller_id") REFERENCES "user_seller"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "shop" ADD CONSTRAINT "shop_address_id_fkey" FOREIGN KEY ("address_id") REFERENCES "address"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "product" ADD CONSTRAINT "product_shop_id_fkey" FOREIGN KEY ("shop_id") REFERENCES "shop"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "product_variant" ADD CONSTRAINT "product_variant_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "product_variant_seo_keywords" ADD CONSTRAINT "product_variant_seo_keywords_product_variant_id_fkey" FOREIGN KEY ("product_variant_id") REFERENCES "product_variant"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "product_variant_images" ADD CONSTRAINT "product_variant_images_product_variant_id_fkey" FOREIGN KEY ("product_variant_id") REFERENCES "product_variant"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "product_variant_videos" ADD CONSTRAINT "product_variant_videos_product_variant_id_fkey" FOREIGN KEY ("product_variant_id") REFERENCES "product_variant"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_seller_id_fkey" FOREIGN KEY ("seller_id") REFERENCES "user_seller"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_product_variant_id_fkey" FOREIGN KEY ("product_variant_id") REFERENCES "product_variant"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_order_id_fkey" FOREIGN KEY ("order_id") REFERENCES "orders"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "customer_order_address" ADD CONSTRAINT "customer_order_address_address_id_fkey" FOREIGN KEY ("address_id") REFERENCES "address"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "customer_order_address" ADD CONSTRAINT "customer_order_address_user_customer_id_fkey" FOREIGN KEY ("user_customer_id") REFERENCES "user_customer"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orders" ADD CONSTRAINT "orders_customer_order_address_id_fkey" FOREIGN KEY ("customer_order_address_id") REFERENCES "customer_order_address"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orders" ADD CONSTRAINT "orders_customer_id_fkey" FOREIGN KEY ("customer_id") REFERENCES "user_customer"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "order_item_review_gallary" ADD CONSTRAINT "order_item_review_gallary_order_item_id_fkey" FOREIGN KEY ("order_item_id") REFERENCES "order_item"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "order_item_review" ADD CONSTRAINT "order_item_review_order_item_id_fkey" FOREIGN KEY ("order_item_id") REFERENCES "order_item"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "customer_response" ADD CONSTRAINT "customer_response_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "user_customer"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "customer_response" ADD CONSTRAINT "customer_response_complaint_id_fkey" FOREIGN KEY ("complaint_id") REFERENCES "complaint"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "user_customer" ADD CONSTRAINT "user_customer_address_id_fkey" FOREIGN KEY ("address_id") REFERENCES "address"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "complaint" ADD CONSTRAINT "complaint_customer_id_fkey" FOREIGN KEY ("customer_id") REFERENCES "user_customer"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "admin_response" ADD CONSTRAINT "admin_response_complaint_id_fkey" FOREIGN KEY ("complaint_id") REFERENCES "complaint"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "admin_response" ADD CONSTRAINT "admin_response_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "user_admin"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cart_item" ADD CONSTRAINT "cart_item_seller_id_fkey" FOREIGN KEY ("seller_id") REFERENCES "user_seller"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cart_item" ADD CONSTRAINT "cart_item_product_variant_id_fkey" FOREIGN KEY ("product_variant_id") REFERENCES "product_variant"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cart_item" ADD CONSTRAINT "cart_item_customer_id_fkey" FOREIGN KEY ("customer_id") REFERENCES "user_customer"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
