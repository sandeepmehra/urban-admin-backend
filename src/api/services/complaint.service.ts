import prisma from "../../../prisma/prisma-client";
import {
    combineArrayDto,
    complaintResponsePostDto,
    complaintsDBDto,
    complaintsResponseDto,
    responsesArrayDto,
} from "../dtos/complaint.dto";
import {
    convertBigInt2String
} from "../helpers/convert.helper";


export const complaintResponsePostService = async (payload: complaintResponsePostDto, admin_id: Number | null) => {
    try {
        const admin_response = await prisma.admin_response.create({
            data: {
                'response': payload.response,
                'complaint_id': Number(payload.complaint_id),
                'user_id': Number(admin_id)
            },
            select: {
                id: true
            }
        });
        return {
            "status": 201,
            "id": String(admin_response.id)
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const complaintsGetService = async () => {
    try {
        const complaintData = await prisma.complaint.findMany({
            orderBy: [{
                'created_on': 'desc'
            }],
            select: {
                'id': true,
                'subject': true,
                'description': true,
                'open': true,
                'customer': {
                    select: {
                        'name': true,
                    }
                },
                'customer_response': {
                    select: {
                        'id': true,
                        'response': true,
                        'created_on': true,
                        'user': {
                            select: {
                                'name': true,
                            }
                        }
                    }
                },
                'admin_response': {
                    select: {
                        'id': true,
                        'response': true,
                        'created_on': true,
                        'user': {
                            select: {
                                'name': true,
                            }
                        }
                    }
    
                }
            }
        });
        return {
            "status": 200,
            "complaints": post_process_complaint(convertBigInt2String(complaintData))
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const complaintPutService = async (payload: {
    id: string
}) => {
    try {
        await prisma.complaint.update({
            where: {
                "id": Number(payload.id)
            },
            data: {
                "open": false,
                "closed_on": new Date()
            }
        });
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_complaint = (complaintData: any) => {
    // console.log(complaintData);
    let complaintData_result: Array <complaintsResponseDto > = [];
    if (complaintData.length > 0) {
        complaintData.forEach((element: complaintsDBDto) => {
            complaintData_result.push({
                'id': element.id,
                'name': element.customer.name,
                'subject': element.subject,
                'description': element.description,
                'status': element.open,
                'responses': post_process_responses(element.customer_response, element.admin_response)
            })
        });
        return complaintData_result;
    }

    throw Error('No complaints available');
}
const post_process_responses = (customerResponses: any, adminResponse: any) => {
    let combineArray: Array < combineArrayDto > = [];

    adminResponse.forEach((element: any) => element['user']['name'] = element['user']['name'] + " [Urban Plants]");

    const combinedResponses = combineArray.concat(customerResponses, adminResponse).sort((a: combineArrayDto, b: combineArrayDto) => (a.created_on < b.created_on) ? 1 : ((b.created_on < a.created_on) ? -1 : 0));
    let resposnes_result: Array < responsesArrayDto > = []
    if (combinedResponses.length > 0) {
        combinedResponses.forEach((element: any) => {
            resposnes_result.push({
                'from': element.user?.name.includes("[Urban Plants]") ? element.user?.name : undefined,
                'datetime': element.created_on,
                'response': element.response
            })
        });
        return resposnes_result;
    }
}
