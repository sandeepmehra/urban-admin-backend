import prisma from "../../../prisma/prisma-client";
import {
    convertBigInt2String,
    convertAddedOntoDuration
} from '../helpers/convert.helper';
import {
    SystemNotificationResponseDto,
    SystemNotificationDBDto
} from "../dtos/systemnotification.dto";

require('dotenv').config();

export const notificationService = async () => {
    try {
        const notifications = await prisma.system_notification.findMany({
            orderBy: [{
                added_on: 'desc'
            }],
            select: {
                'id': true,
                'title': true,
                'message': true,
                'added_on': true
            },
            take: 10
        })

        return {
            "status": 200,
            "notification": post_process_notifications(convertBigInt2String(notifications))
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


const post_process_notifications = (notification_result: any) => {
    if (notification_result.length > 0) {
        let system_notifications: Array < SystemNotificationResponseDto > = [];
        notification_result.forEach((element: SystemNotificationDBDto) => {
            system_notifications.push({
                'id': element.id,
                'title': element.title,
                'message': element.message,
                'duration': convertAddedOntoDuration(element.added_on)
            });
        });
        return system_notifications;
    }
    throw Error('No notification available');
}