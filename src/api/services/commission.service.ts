import prisma from "../../../prisma/prisma-client";
import {
    UserSellerDBDto,
    UserSellerResponseDto
} from "../dtos/userseller.dto";
import {
    convertBigInt2String,
    StateAbbreviation
} from "../helpers/convert.helper";
let util = require("util");

util.inspect.defaultOptions.depth = null
util.inspect.defaultOptions.colors = true
util.inspect.defaultOptions.showHidden = true
require('dotenv').config();

export const commissionPostService = async (commission_percentage: number) => {
    try {
        await prisma.user_seller_default_commission.create({
            data: {
                'commission_percentage': commission_percentage
            }
        });
        return {
            "status": 204,
            "message": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const commissionGetService = async () => {
    try {
        const user_seller = await prisma.user_seller.findMany({
            orderBy: [{
                created_on: 'desc'
            }],
            take: 10,
            select: {
                'id': true,
                'name': true,
                'email': true,
                'mobile_number': true,
                'verification': {
                    select: {
                        'verified': true
                    }
                },
                'shop': {
                    select: {
                        'address': {
                            select: {
                                'city': true,
                                'state': true,
                            }
                        }
                    }
                },
            },
        });

        return {
            "status": 200,
            "sellers": post_process_user_seller(convertBigInt2String(user_seller))
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const commissionPutService = async (payload: {
    commission_percentage: number;sellers: Array < string >
}) => {
    try {
        await prisma.user_seller.updateMany({
            where: {
                "id": {
                    in: payload.sellers.map(Number)
                },
            },
            data: {
                "commission_percentage": payload.commission_percentage,
                "commission_added_on": new Date()
            }
        });
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_user_seller = (user_seller_result: any) => {
    let sellers: Array < UserSellerResponseDto > = [];
    if (user_seller_result.length > 0) {
        user_seller_result.forEach((element: UserSellerDBDto) => {
            sellers.push({
                'id': element.id,
                'name': element.name,
                'email': element.email,
                'mobileNumber': element.mobile_number,
                'location': formatShopAddress(element.shop),
                'verified': element.verification === null ? false : element.verification.verified
            });
        });
        return sellers;
    }
    throw Error('No user seller available');
}

const formatShopAddress = (payload: any) => {
    if (payload === null || payload === undefined) {
        return `${null}, ${null}`
    }
    return `${payload.address.city}, ${StateAbbreviation[ payload.address.state]!==undefined?StateAbbreviation[ payload.address.state]: 'NA'}`
}