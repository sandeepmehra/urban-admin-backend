import prisma from "../../../prisma/prisma-client";
import { orderDBDto, orderItemsDBDto, orderItemsResponseDto, orderResponseDto } from "../dtos/order.dto";
import {
    convertBigInt2String,
} from "../helpers/convert.helper";
import { get_order_status } from "../helpers/status.helper";
require('dotenv').config();
let util = require("util");
util.inspect.defaultOptions.depth = null
util.inspect.defaultOptions.colors = true
util.inspect.defaultOptions.showHidden = true


export const orderGetService = async () => {
    try {
        const order_result:any = await prisma.orders.findMany({
            where: {
                payment_txn_status: 'TXN_SUCCESS'
            },
            orderBy: [{
                placed_on:'desc'
            }],
            select: {
                'id': true,
                'placed_on': true,
                'total_price': true,
                'payment_mode': true,
                'customer': {
                    select: {
                        'name': true
                    }
                },
                'customer_order_address': {
                    select: {
                        'name': true, 
                        'address': {
                            select: {
                               'street': true,
                               'city': true,
                               'state': true,
                               'pincode': true 
                            }
                        }
                    }
                },
                'order_items': {
                    select: {
                        'quantity': true,
                        'price_per_unit': true,
                        'status': true,
                        'delivery_date': true,
                        'dispatch_date': true,
                        'product_variant': {
                            select: {
                                'sku': true,
                                product: {
                                    select: {
                                        'name': true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
        return {
            "status": 200,
            "orders": post_process_orders(convertBigInt2String(order_result))
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}
const post_process_orders = (orders: any) => {
    let orderArr : Array<orderResponseDto> = [];
    if(orders.length > 0) {
        orders.forEach((element: orderDBDto) => {
            orderArr.push({
                "id": element.id,
                "placed_on": element.placed_on,
                "customer": element.customer.name,
                "payment_mode": element.payment_mode,
                "total": String(element.total_price),
                "status": String(get_order_status(element.order_items)),
                "address": `${element.customer_order_address.name}, ${element.customer_order_address.address.street}, ${element.customer_order_address.address.city}, ${element.customer_order_address.address.state}, ${element.customer_order_address.address.pincode}`,
                "order_items": post_process_order_items(element.order_items)
            });
        });
        return orderArr;
    }
    throw Error('No order available');
    
}


const post_process_order_items = (order_item_result: any) => {
    let orderItem : Array <orderItemsResponseDto> = [];
    if(order_item_result.length > 0) {
        order_item_result.forEach((element: orderItemsDBDto)=> {
            orderItem.push({
                "product_name": element.product_variant.product.name,
                "sku": element.product_variant.sku,
                "quantity": String(element.quantity),
                "dispatch_date": element.dispatch_date !== null ? element.dispatch_date: 'NA',
                "delivery_date": element.delivery_date === null ? 'NA': element.delivery_date,
                "status": element.status !== null ? element.status: 'Shipped',
                "total": post_process_multiply(element.quantity, element.price_per_unit)
            });
        });
        return orderItem;
    }
    throw Error('No order-item available');
}
const post_process_multiply = (a: number, b: number) => {
    return String(Math.round((a*b) * 100) / 100);
}