import express from 'express';
import prisma from "../../../prisma/prisma-client";
import logger from '../../config/logger.config';
import {
    LoginNetworkDto
} from '../dtos/auth.dto';
import {
    generateAccessToken,
    generateRefreshToken,
    verifyJWT
} from "../helpers/jwt.helper";

require('dotenv').config();

export const login = async (userDto: LoginNetworkDto) => {
    const user = await prisma.user_admin.findMany({
        where: {
            AND: [{
                email: {
                    equals: userDto.email
                },
                password: {
                    equals: userDto.password
                }
            }]

        },
        select: {
            name: true
        },
    });

    if (user[0]) {
        logger('info', 'POST', '/login', 200, `${userDto.email} successfully logged in.`);
        return {
            access_token: generateAccessToken(user[0].name, userDto.email),
            refresh_token: generateRefreshToken(user[0].name, userDto.email),
        };
    }
    logger('error', 'POST', '/login', 401, `User: ${userDto.email} not found.`);
    return {}
};

export const refresh = async (req: express.Request, res: express.Response) => {
    const access_token = req.headers.authorization !== undefined ? req.headers.authorization : '';
    verifyJWT(req.body['refresh_token'], '/refresh', 'POST', 'refresh', res, (rt_name: string, rt_sub: string)=>{
        verifyJWT(access_token, '/refresh', 'POST', 'access', res, (at_name: string, at_sub: string)=>{
            if (at_name === rt_name && at_sub === rt_sub) {
                return res.status(200).setHeader('Authorization', generateAccessToken(at_name, at_sub)).json({});
            } else {
                logger('error', 'POST', '/refresh', 401, 'Different "name" and "sub" claims in access token and refresh token');
                return res.status(401).json({"message": process.env.RESPONSE_UNAUTHORIZED});
            }
        }, true);
    });
};