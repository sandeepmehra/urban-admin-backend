import prisma from "../../../prisma/prisma-client";
import { sellerDbDto, SellerDto, sellerResponseDto } from "../dtos/seller.dto";
import { get_status } from "../helpers/status.helper";

export const sellerGetService = async () => {
    try {
        const seller_result = await prisma.user_seller.findMany({
            orderBy: [{
                created_on: 'desc'
            }],
            select: {
                "id": true,
                "name": true,
                "email": true,
                "created_on": true,
                "mobile_number": true,
                "commission_percentage": true,
                "shop": {
                    select: {
                        "name": true,
                        "logo": true,
                        "description": true,
                        "url": true,
                        "wa_number": true, 
                        "mobile_number": true,
                        "address": {
                            select: {
                                "street": true,
                                "city": true,
                                "state": true,
                                "pincode": true
                            }
                        },
                        "products": {
                            select: {
                                "name": true
                            }
                        }
                    }
                },
                "verification": {
                    select: {
                        "aadhar_card_url": true,
                        "pan_card_url": true,
                        "selfie_url": true,
                        "bank_document_url": true,
                        "gst_number": true,
                        "action_taken": true,
                        "verified": true,
                    }
                }
            }
        })
      
        return {
            "status": 200,
            sellers: post_process_seller(seller_result)
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
    
}

export const sellerApporveRejectPutService = async (id: string, verified: boolean) => {
    try {
        await prisma.user_seller_verification.update({
            where: {
                "id": Number(id)
            },
            data: {
                "verified": verified,
                "action_taken": true,
                "action_taken_on": new Date()
            }
        });
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const sellerPutService = async(body: SellerDto, seller_id: number) =>{
    try {
        await prisma.user_seller.update({
            where: {
                "id": seller_id
            },
            data: {
                "name": body.seller_name,
                "email": body.email,
                "mobile_number": body.mobile_number
            }
        });
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_seller = (sellers: any) => {
    let sellersArr: Array<sellerResponseDto> = [];
    if(sellers.length > 0) {
        sellers.forEach((element: sellerDbDto)=>{
            sellersArr.push({
                "id": String(element.id),
                "seller_name": element.name,
                "email": element.email,
                "created_at": String(element.created_on),
                "mobile_number": String(element.mobile_number),
                "commission_percentage": String(element.commission_percentage),
                "status": get_status(element.verification.verified, element.verification.action_taken),
                "shop": {
                    "name": element.shop.name,
                    "logo": element.shop.logo,
                    "description": element.shop.description,
                    "url": element.shop.url,
                    "wa_number": String(element.shop.wa_number),
                    "mobile_number": String(element.shop.mobile_number),
                    "street": element.shop.address.street,
                    "city": element.shop.address.city,
                    "state": element.shop.address.state,
                    "pincode": String(element.shop.address.pincode),
                    "numberOfProducts": String(element.shop.products.length)
                },
                "verification": {
                    "aadhar_card": element.verification.aadhar_card_url,
                    "pan_card": element.verification.pan_card_url,
                    "selfie": element.verification.selfie_url,
                    "bank_document": element.verification.bank_document_url,
                    "gst_number": element.verification.gst_number,
                    "action_taken": String(element.verification.action_taken),
                    "verified": String(element.verification.verified)
                }
            })
        });
        return sellersArr;
    }
    throw Error("sellers not available")
}
