import prisma from "../../../prisma/prisma-client";

export const unitGetService = async () => {
    try {
        const unit_result = await prisma.product_measurement_unit.findMany({
            select: {
                "unit": true
            }
        })
        return {
            "status": 200,
            units: post_proces_unit(unit_result)
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}
const post_proces_unit = (units: Array<{unit:string}>|undefined) => {
    let unitList : Array<string> = [];
    if(units !== undefined && units.length > 0) {
        units.forEach((element: {unit: string}) => {
            unitList.push(element.unit);
        });
        return unitList;
    }
    throw Error("units not available")
}