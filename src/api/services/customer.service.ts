import prisma from "../../../prisma/prisma-client";
import { UserCustomerDBDto, OrderItemsDBDto, UserCustomerResponseDto, OrderItemDBDto, UserOrderItemResponseDto } from "../dtos/usercustomer.dto";
import { convertBigInt2String, StateAbbreviation } from "../helpers/convert.helper";


export const customerGetService = async () => {
    try {
        const user_customer = await prisma.user_customer.findMany({
            orderBy: [{
                created_on: 'desc'
            }],
            take: 5,
            select: {
                'id': true,
                'name': true,
                'email': true,
                'mobile_number': true,
                'suspended': true,
                'customer_order_address':{
                    select:{
                        'address': {
                            select: {
                                'city': true,
                                'state': true,
                            }
                        },
                    }
                },
                'orders': {
                    where: {
                        payment_txn_status: 'TXN_SUCCESS'
                    },
                    select: {
                        'placed_on': true,
                        'order_items': {
                            select: {
                                'id': true,
                                'product_variant':{
                                    select: {
                                        'sku': true,
                                        'product': {
                                            select: {
                                                'name': true,
                                            }
                                        },
                                    }
                                },
                                'order_item_review': {
                                    select:{
                                        'rating': true
                                    }
                                },
                            }
                        }
                    }
                }
            },
            
        });
        return {
            "status": 200,
            "customers": post_process_user_customer(convertBigInt2String(user_customer))
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const customerPutService = async (payload: {
    customer_id: string; suspend: boolean
}) => {
    try {
        console.log("customer put is called", payload.customer_id, payload.suspend);
        await prisma.user_customer.update({
            where: {
                "id": Number(payload.customer_id)
            },
            data: {
                "suspended": payload.suspend
            }
        });
        console.log("user_customer is updated");
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_user_customer = (user_customer_result: any) => {
    let customers: Array < UserCustomerResponseDto > = [];
    if (user_customer_result.length > 0) {
        user_customer_result.forEach((element: UserCustomerDBDto) => {
            customers.push({
                'id': element.id,
                'name': element.name,
                'email': element.email,
                'mobileNumber': element.mobile_number,
                'location': formatAddress(element.customer_order_address[0]?.address),
                'suspended': element.suspended,
                'order_items': post_process_orders(element.orders)
            });
        });
        return customers;
    }
    throw Error('No user customer available');
}

const post_process_orders = (orders_result: any) => {
    let orders: Array< any > = [];
    if(orders_result.length > 0) {
        orders_result.sort((a: OrderItemsDBDto,b: OrderItemsDBDto)=>(a.placed_on > b.placed_on) ? 1 : ((b.placed_on > a.placed_on) ? -1 : 0))
        orders_result.forEach((item: OrderItemsDBDto) =>{
            orders.push(
                post_process_order_items(item.order_items)
            )
            
        });
        return orders.flat();
    }
}

const post_process_order_items = (orders_items_result: any) => {
    let order_items: Array <UserOrderItemResponseDto> = [];
    if (orders_items_result.length > 0) {
        orders_items_result.forEach((item: OrderItemDBDto) => {
            order_items.push({
                'order_item_id': String(item.id),
                'name': item.product_variant.product.name,
                'sku': item.product_variant.sku,
                'rating': item.order_item_review?.rating,
            })
        })
        return order_items;
    }
}

const formatAddress = (payload: any) => {
    if (payload === null || payload === undefined) {
        return `${null}, ${null}`
    }
    return `${payload.city}, ${StateAbbreviation[ payload.state]!==undefined ? StateAbbreviation[ payload.state ] : 'NA'}`
}