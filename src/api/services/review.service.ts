import prisma from "../../../prisma/prisma-client";
let util = require("util");

util.inspect.defaultOptions.depth = null
util.inspect.defaultOptions.colors = true
util.inspect.defaultOptions.showHidden = true
require('dotenv').config();

export const reviewGetService = async (customer_id: number) => {
    try {
        const review_results: any = await prisma.user_customer.findMany({
            where: {
                'id': customer_id,
                orders: {
                    some: {
                        order_items: {
                            some: {
                                order_item_review: {
                                    action_taken: false
                                }
                            }
                        }
                    }
                }
            },
            select: {
                orders: {
                    select: {
                        order_items: {
                            select: {
                                order_item_review: {
                                    select: {
                                        "id": true,
                                        "review": true,
                                        "action_taken": true,
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
        return {
            "status": 200,
            "review": post_process_review_result(review_results)
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


export const reviewPutService = async (payload: {
    id: string;approved: boolean
}) => {
    try {
        await prisma.order_item_review.update({
            where: {
                "id": Number(payload.id)
            },
            data: {
                "approved": payload.approved,
                "action_taken": true
            }
        });
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_review_result = (review_result: any) => {
    let reviewArr: Array < any > = [] ;
    if (review_result.length === 0) {
        return {}
    }

    review_result[0].orders.forEach((element_1: any) => {
        element_1.order_items.forEach((element_2: any) => {
            if (element_2.order_item_review != null && !element_2.order_item_review.action_taken) {
                reviewArr.push(element_2.order_item_review);
            }
        });
    });

    let review = reviewArr.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))[0];
    
    return {
        "id": String(review.id),
        "review": review.review
    }
}