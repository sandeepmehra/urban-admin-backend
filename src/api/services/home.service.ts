import prisma from "../../../prisma/prisma-client";
import { convertBigInt2String, convertAddedOntoDuration } from '../helpers/convert.helper'; 
import { SystemNotificationResponseDto, SystemNotificationDBDto } from "../dtos/systemnotification.dto";
import { get_order_status } from "../helpers/status.helper";
import { orderHomeDBDto, orderHomeResponseDto } from "../dtos/order.dto";
require('dotenv').config();

export const homeService = async () => {
    const user_seller_verifications = prisma.user_seller_verification.groupBy({
        by: ["verified", 'action_taken'],
        _count: {
            verified: true
        }
    });
    const product_variants = prisma.product_variant.groupBy({
        by: ['approved', 'action_taken'],
        _count: {
            approved: true
        }
    });
    
    const complaints = prisma.complaint.groupBy({
        by: ['open'],
        _count: {
            open: true
        }  
    });
    // const orders = prisma.orders.findMany({
    //     orderBy: [{ placed_on: 'desc' }],
    //     select: { 
    //         'id': true,
    //         'order_items': {
    //             select: {
    //                 'status': true
    //             }
    //         }
    //     },
    //     take: 5
    // });

    const latest_commission = prisma.user_seller_default_commission.findMany({
        orderBy: [{ added_on: 'desc' }],
        select: { 'commission_percentage': true },
        take: 1
    });
    const notification = prisma.system_notification.findMany({
        orderBy: [{ added_on: 'desc'}],
        select: { 'title': true, 'added_on': true},
        take: 5
    })

    try {
        const [
            // orders_result, 
            latest_commission_result, 
            user_seller_verifications_result, 
            complaints_result, 
            product_variants_result,
            notification_result
        ] = await Promise.all([
                // orders,
                latest_commission, 
                user_seller_verifications, 
                complaints, 
                product_variants, 
                notification
            ]);

        return  {
                    "status": 200,
                    "verification": post_process_user_seller_verifications(user_seller_verifications_result), 
                    "inventory": post_process_product_variants(product_variants_result),
                    "complaints": post_process_complaints(complaints_result), 
                    // "orders": post_process_orders(convertBigInt2String(orders_result)), 
                    "current_commission": post_process_latest_commission(latest_commission_result), 
                    "notification": post_process_notifications(notification_result)
                };
                
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}
const post_process_orders = (orders: any) => {
    let orderStatus: Array<orderHomeResponseDto> = [];
    if(orders.length > 0){
        orders.forEach((element: orderHomeDBDto)=> {
            orderStatus.push({'id': element.id, 'status': get_order_status(element.order_items)});
        })
        return orderStatus;
    }
    throw Error('No orders available');
}
// Pending: action_taken (False), approved(False)
// Rejected: action_taken (True), approved(False)
// Approved: action_taken (True), approved(True)
const post_process_user_seller_verifications = (current_verification: any) =>{
    // console.log("current_verification", current_verification);
    let pending_count: number|undefined;
    let approved_count: number|undefined;
    let reject_count: number|undefined;
    if(current_verification.length > 0){
        current_verification.forEach((element:{_count:{verified: number}, verified:boolean, action_taken: boolean})=>{
            if(element.action_taken === false && (element.verified === false || element.verified === true)){
                pending_count = element._count.verified 
            }else if(element.action_taken === true && element.verified === true){
                approved_count = element._count.verified
            }else{
                reject_count = element._count.verified
            }
        });
        return {
                "pending": pending_count !== undefined ? pending_count : 0,
                "approved": approved_count !== undefined ? approved_count : 0
            }
    }
}
const post_process_product_variants = (current_product_variants: any) =>{
    // console.log("current_product_variants", current_product_variants);
    let pending_count: number|undefined;
    let approved_count: number|undefined;
    let reject_count: number|undefined;
    if(current_product_variants.length > 0){
        current_product_variants.forEach((element:{_count:{approved: number}, approved:boolean, action_taken: boolean})=>{
            if(element.action_taken === false && (element.approved === false || element.approved === true)){
                pending_count = element._count.approved 
            }else if(element.action_taken === true && element.approved === true){
                approved_count = element._count.approved
            }else{
                reject_count = element._count.approved
            }
        });
        return {
                "pending": pending_count !== undefined ? pending_count : 0,
                "approved": approved_count !== undefined ? approved_count : 0
            }
    }
}
const post_process_complaints = (current_complaints: any) => {
    // console.log("current_complaints", current_complaints);
    let opened_count: number|undefined;
    let closed_count: number|undefined;
    if(current_complaints.length > 0){
        current_complaints.forEach((element:{_count:{open: number}, open:boolean})=>{
            if(element.open === false){
                closed_count = element._count.open 
            }else if(element.open === true){
                opened_count = element._count.open
            }else{
                opened_count = 0;
                closed_count = 0;
            }
        });
        return {
                "opened": opened_count !== undefined ? opened_count : 0,
                "closed": closed_count !== undefined ? closed_count : 0
            }
    }
}
const post_process_latest_commission = (current_commission: any)=>{
    if(current_commission.length == 1){
        return current_commission[0].commission_percentage;
    }
    throw Error('No common commission available');
}

const post_process_notifications = (notification_result: any)=>{
let system_notifications: Array<SystemNotificationResponseDto> = [];
if(notification_result.length > 0){
        notification_result.forEach((element: SystemNotificationDBDto) => {
            system_notifications.push({'title': element.title, 'duration': convertAddedOntoDuration(element.added_on)});
        });
        return system_notifications;
    }
    throw Error('No notification available');
}