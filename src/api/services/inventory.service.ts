import prisma from "../../../prisma/prisma-client";
import { inventoryDbDto, inventoryResponseDto, inventoryUpdateDto } from "../dtos/Inventory.dto";
import { get_status } from "../helpers/status.helper";

export const inventoryGetService = async () => {
    try {
        const product_result = await prisma.product_variant.findMany({
            orderBy: [{
                added_on: 'desc'
            }],
            select: {
                "id": true,
                "price": true,
                "quantity": true,
                "color": true,
                "approved": true,
                "action_taken": true,
                "measurement": true,
                "measurement_unit": true,
                "sku": true,
                "hsn_code": true,
                "country_of_origin": true,
                "product": {
                    select: {
                        "name": true,
                        "product_category": true,
                        "shop": {
                            select: {
                                "name": true,
                            }
                        }
                    }
                    
                },
                "product_variant_seo_keywords": {
                    select: {
                        "seo_keyword": true,
                    }
                },
                "product_variant_images": {
                    select: {
                        "url": true,
                    }
                },
                "product_variant_videos": {
                    select: {
                        "url": true,
                    }
                }, 
                "order_items": {
                    select:{
                        "order_item_review": {
                            select: {
                                "rating": true
                            }
                        }
                    }
                }
            }
        })
        return {
            "status": 200,
            products: post_process_product(product_result)
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const inventoryApproveRejectPutService = async (id: string, approved: boolean) => {
    try {
        await prisma.product_variant.update({
            where: {
                "id": Number(id)
            },
            data: {
                "approved": approved,
                "action_taken": true,
                "action_taken_on": new Date()
            }
        });
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const inventoryPutService = async(body: inventoryUpdateDto, id: number) => {
    try {
        if(body.videos !== undefined){
            await prisma.product_variant_videos.deleteMany({
                where: {
                    AND:[
                        {product_variant_id: Number(id)},
                        {OR:[{url: {in: body.videos}}]}
                    ]
                }
            })
        }
        if(body.images !== undefined){
            await prisma.product_variant_images.deleteMany({
                where: {
                    AND:[
                        {product_variant_id: Number(id)},
                        {OR:[{url:{in: body.images}}]}
                    ]
                }
            })
        }
        if(body.seo_keywords !== undefined ) {
            if(body.seo_keywords[0] !== undefined && Object.entries(body.seo_keywords[0]).length > 0){
                await prisma.product_variant_seo_keywords.deleteMany({
                    where: {
                        AND:[
                            {product_variant_id: Number(id)},
                            {OR:[{seo_keyword : {in: body.seo_keywords[0] }}]}
                        ]
                    }
                })
            }
        }
        if(body.seo_keywords !== undefined){
            if(body.seo_keywords[1] !== undefined && Object.entries(body.seo_keywords[1]).length > 0){
                let seo_keywords_data : Array<{product_variant_id: number; seo_keyword:string}> = [];
                body.seo_keywords[1].forEach((seo_keyword: string) => {
                    seo_keywords_data.push({
                        product_variant_id: Number(id),
                        seo_keyword : seo_keyword
                    })
                });

                await prisma.product_variant_seo_keywords.createMany({
                    data: seo_keywords_data
                });
            }
        }
        
        await prisma.product_variant.update({
            where: {
                "id": Number(id)
            },
            data: {  
                "quantity": body.quantity,
                "color": body.color, 
                "measurement": body.measurement,
                "measurement_unit": body.measurement_unit,
                "sku": body.sku,
                "hsn_code": body.hsn_code,
                "country_of_origin": body.country_of_origin,
                "product": {
                    update: {
                        "name": body.product_name,
                        "product_category": body.category
                    }
                },
                "action_taken_on": new Date()
            }
        });
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_product = (products: any) => {
    let productsArr: Array<inventoryResponseDto> = [];
    if(products.length > 0) {
        products.forEach((element: inventoryDbDto)=>{
            productsArr.push({
                "id": String(element.id),
                "action_taken": String(element.action_taken),
                "seller_name": element.product.shop.name,
                "product_name": element.product.name,
                "sku": element.sku,
                "price": String(Math.round((element.price * 100)/100)),
                "status": get_status(element.approved, element.action_taken),
                "rating": String(get_rating(element.order_items)),
                "category": element.product.product_category,
                "quantity": String(element.quantity),
                "hsn_code": element.hsn_code,
                "country_of_origin": element.country_of_origin,
                "measurement": element.measurement,
                "measurement_unit": element.measurement_unit,
                "color": element.color,
                "seo_keywords": get_seo_keywords_array(element.product_variant_seo_keywords),
                "images": get_images_array(element.product_variant_images),
                "videos": get_videos_array(element.product_variant_videos)
            })
        });
        return productsArr;
    }
    throw Error("product not available")
}

const get_seo_keywords_array = (keywords: any) => {
    let keywordsArr: Array<string> = [];
    if(keywords.length > 0) {
        keywords.forEach((element: any)=>{
            keywordsArr.push(element.seo_keyword)
        })
        return keywordsArr;
    }
    return [];
} 
const get_images_array = (images: any) => {
    let imagesArr: Array<string> = [];
    if(images.length > 0) {
        images.forEach((element: any)=>{
            imagesArr.push(element.url)
        })
        return imagesArr;
    }
    return [];           
} 
const get_videos_array = (videos: any) => {
    let videosArr: Array<string> = [];
    if(videos.length > 0) {
        videos.forEach((element: any)=>{
            videosArr.push(element.url)
        })
        return videosArr;
    }
    return [];
} 
const get_rating = (orderItems: any) => {
    let ratingArr: Array<number> = [];
    if(orderItems.length > 0) {
        orderItems.forEach((element: any)=>{
            if(element.order_item_review !== null){
                ratingArr.push(element.order_item_review.rating)
            }    
        });
        
        let average: any = ratingArr.reduce((a:number, b:number) => a + b, 0) / ratingArr.length; 
        return (!Number.isNaN(average) === true ? average : 0);   
    }
    return 0;
}