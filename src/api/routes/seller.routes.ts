import express from 'express';
import {
    RoutesConfig
} from '../../config/routes.config';
import {
    accessTokenVerificationMiddleware
} from '../middlewares/auth.middleware';
import {
    SingleBodyValidationMiddleware
} from '../middlewares/common.middleware';
import {
    body, param
} from 'express-validator';
import { sellerApprovePutController, sellerGetController, sellerRejectPutController, sellerPutController } from '../controllers/seller.controller';



export class SellerRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'SellerRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/sellers', [accessTokenVerificationMiddleware, sellerGetController]);
        this.app.put('/seller/approve', [
            accessTokenVerificationMiddleware,
            body('id').notEmpty().withMessage('Please enter a valid seller').bail()
            .isString().withMessage('Please enter a valid seller'),
            SingleBodyValidationMiddleware,   
            sellerApprovePutController
        ]);
        this.app.put('/seller/reject', [
            accessTokenVerificationMiddleware,
            body('id').notEmpty().withMessage('Please enter a valid seller').bail()
            .isString().withMessage('Please enter a valid seller'),
            SingleBodyValidationMiddleware,   
            sellerRejectPutController
        ]);
        this.app.put('/seller/:seller_id', [
            accessTokenVerificationMiddleware,
            param('seller_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid seller').bail()
            .toInt(),
            SingleBodyValidationMiddleware, 
            body('email').optional().isEmail().withMessage('Please enter a valid seller'),
            SingleBodyValidationMiddleware,
            body('name').optional().notEmpty().withMessage('Please enter a valid seller').bail()
            .isString().withMessage('Please enter a valid seller'),
            SingleBodyValidationMiddleware,
            body('mobile_number').optional().notEmpty().withMessage('Please enter a valid seller').bail()
            .isString().withMessage('Please enter a valid seller').bail()
            .isMobilePhone('en-IN').withMessage('Please enter a valid seller')
            .toInt(),
            SingleBodyValidationMiddleware,
            sellerPutController
        ])
        return this.app;
    }
}