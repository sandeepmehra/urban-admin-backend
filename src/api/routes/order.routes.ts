import express from 'express';
import { RoutesConfig } from '../../config/routes.config';
import { orderGetController } from '../controllers/order.controller';
import { accessTokenVerificationMiddleware } from '../middlewares/auth.middleware';

export class OrderRoutes extends RoutesConfig{
    constructor(app: express.Application){
        super(app, 'OrderRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/orders',[ accessTokenVerificationMiddleware, orderGetController]);
        return this.app;
    }

};