import express from 'express';
import {
    RoutesConfig
} from '../../config/routes.config';
import {
    accessTokenVerificationMiddleware
} from '../middlewares/auth.middleware';
import {
    SingleBodyValidationMiddleware
} from '../middlewares/common.middleware';
import {
    body, param
} from 'express-validator';
import { inventoryGetController, inventoryApprovePutController, inventoryRejectPutController, inventoryPutController } from '../controllers/inventory.controller';
import { checkInventoryUnitsExists } from '../validations/inventory.validation';



export class InventoryRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'InventoryRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/inventory', [accessTokenVerificationMiddleware, inventoryGetController]);
        this.app.put('/inventory/approve', [
            accessTokenVerificationMiddleware,
            body('id').notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,   
            inventoryApprovePutController
        ]);
        this.app.put('/inventory/reject', [
            accessTokenVerificationMiddleware,
            body('id').notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,   
            inventoryRejectPutController
        ]);
        this.app.put('/inventory/:product_variant_id', [
            accessTokenVerificationMiddleware,
            param('product_variant_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid inventory').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            body('product_name').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('category').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('quantity').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory').bail()
            .custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid inventory').bail()
            .toInt(),
            SingleBodyValidationMiddleware,

            body('measurement_unit').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory').bail()
            .custom(async(value) => await checkInventoryUnitsExists(value)).withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,

            body('country_of_origin').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('color').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('measurement').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('hsn_code').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('sku').optional().notEmpty().withMessage('Please enter a valid inventory').bail()
            .isString().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('images').optional().isArray().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('videos').optional().isArray().withMessage('Please enter a valid inventory'),
            SingleBodyValidationMiddleware,
            body('seo_keywords').optional().isObject().withMessage('Please enter a valid inventory').bail(),
            SingleBodyValidationMiddleware,
            inventoryPutController
        ])
        return this.app;
    }
}