import express from 'express';
import {
    RoutesConfig
} from '../../config/routes.config';
import {
    accessTokenVerificationMiddleware
} from '../middlewares/auth.middleware';
import {
    SingleBodyValidationMiddleware
} from '../middlewares/common.middleware';
import {
    body
} from 'express-validator';
import { customerPutController, customerGetController } from '../controllers/customer.controller';

export class CustomerRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'CustomerRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/customer', [accessTokenVerificationMiddleware, customerGetController])
        this.app.put('/customer', [
            accessTokenVerificationMiddleware,
            body('customer_id').notEmpty().withMessage('Please enter a valid customer').bail()
            .isString().withMessage('Please enter a valid customer'),
            SingleBodyValidationMiddleware,
            body('suspend').notEmpty().withMessage('Please provide a valid statement').bail()
            .custom((value) => typeof value === "boolean").withMessage('Please provide a valid statement'),
            SingleBodyValidationMiddleware,
            customerPutController
        ]);
        return this.app;
    }
}