import express from 'express';
import { RoutesConfig } from '../../config/routes.config';
import { notificationController } from '../controllers/notification.controller';
import { accessTokenVerificationMiddleware } from '../middlewares/auth.middleware';

export class NotificationRoutes extends RoutesConfig{
    constructor(app: express.Application){
        super(app, 'NotificationRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/notification', [ accessTokenVerificationMiddleware, notificationController ]);
        return this.app;
    }

};