import express from 'express';
import {
    RoutesConfig
} from '../../config/routes.config';
import {
    accessTokenVerificationMiddleware
} from '../middlewares/auth.middleware';
import {
    SingleBodyValidationMiddleware
} from '../middlewares/common.middleware';
import {
    body, param
} from 'express-validator';

import {reviewGetController, reviewPutController } from '../controllers/review.controller';

export class ReviewRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'ReviewRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/review/:customer_id', [
            accessTokenVerificationMiddleware, 
            param('customer_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid review').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            reviewGetController
        ]);
        this.app.put('/review', [
            accessTokenVerificationMiddleware,
            body('id').notEmpty().withMessage('Please enter a valid review').bail()
            .isString().withMessage('Please enter a valid review'),
            SingleBodyValidationMiddleware,
            body('approved').notEmpty().withMessage('Please provide a valid statement').bail()
            .custom((value) => typeof value === "boolean").withMessage('Please provide a valid statement'),
            SingleBodyValidationMiddleware,   
            reviewPutController
        ]);
        return this.app;
    }
}