import express from 'express';
import {
    RoutesConfig
} from '../../config/routes.config';
import { unitGetController } from '../controllers/unit.controller';
import {
    accessTokenVerificationMiddleware
} from '../middlewares/auth.middleware';



export class UnitRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'UnitRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/units', [accessTokenVerificationMiddleware, unitGetController]);
        return this.app;
    }
}