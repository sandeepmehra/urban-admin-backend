import express from 'express';
import { body, header} from 'express-validator';
import { isAuthHeaderValid, isRefreshTokenBodyValid} from '../validations/auth.validation';
import {SingleBodyValidationMiddleware} from "../middlewares/common.middleware";
import { RoutesConfig } from "../../config/routes.config";
import { loginController, refreshController } from '../controllers/auth.controller';

export class AuthRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'AuthRoutes');
    }
    configureRoutes(): express.Application {
        this.app.post('/login', [
            body('email').isEmail().withMessage('Please enter a valid Email'),
            SingleBodyValidationMiddleware,
            body('password').notEmpty().withMessage('Please enter a valid Password').bail().isString().withMessage('Please enter a valid Password').bail()
            .isLength({ min: 6 }).withMessage('Password should be atleast 6 character long'),
            SingleBodyValidationMiddleware,
            loginController
        ]);

        this.app.post('/refresh', [
            header('authorization').custom(isAuthHeaderValid).withMessage('Please send a valid access token'),
            SingleBodyValidationMiddleware,
            body('refresh_token').custom(isRefreshTokenBodyValid).withMessage('Please send a valid refresh token'),
            SingleBodyValidationMiddleware,
            header('authorization').customSanitizer((token: string)=> token.split(' ')[1]),
            refreshController
        ]);

        return this.app;
    }
}
