import express from 'express';
import {
    RoutesConfig
} from '../../config/routes.config';
import {
    accessTokenVerificationMiddleware
} from '../middlewares/auth.middleware';
import {
    SingleBodyValidationMiddleware
} from '../middlewares/common.middleware';
import {
    body
} from 'express-validator';
import { complaintPutController, complaintsGetController, complaintResponsePostController } from '../controllers/complaint.controller';

export class ComplaintRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'ComplaintRoutes');
    }
    configureRoutes(): express.Application {
        this.app.post('/complaint/response', [
            accessTokenVerificationMiddleware,
            body('complaint_id').notEmpty().withMessage('Please enter a valid complaint').bail()
            .isString().withMessage('Please enter a valid complaint'),
            SingleBodyValidationMiddleware,
            body('response').notEmpty().withMessage('Please enter a valid complaint').bail()
            .isString().withMessage('Please enter a valid complaint'),
            SingleBodyValidationMiddleware,
            body('email').isEmail().withMessage('Please enter a valid complaint'),
            SingleBodyValidationMiddleware,
            complaintResponsePostController
        ]);
        this.app.get('/complaints', [accessTokenVerificationMiddleware, complaintsGetController]);
        this.app.put('/complaint/close', [
            accessTokenVerificationMiddleware,
            body('id').notEmpty().withMessage('Please enter a valid complaint').bail()
            .isString().withMessage('Please enter a valid complaint'),
            SingleBodyValidationMiddleware,
            complaintPutController
        ]);
        return this.app;
    }
}