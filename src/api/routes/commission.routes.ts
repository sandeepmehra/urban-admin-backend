import express from 'express';
import {
    RoutesConfig
} from '../../config/routes.config';
import {
    accessTokenVerificationMiddleware
} from '../middlewares/auth.middleware';
import {
    SingleBodyValidationMiddleware
} from '../middlewares/common.middleware';
import {
    body
} from 'express-validator';
import {
    commissionPostController,
    commissionGetController,
    commissionPutController
} from '../controllers/commission.controller';

export class CommissionRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'CommissionRoutes');
    }
    configureRoutes(): express.Application {
        this.app.post('/commissions', [
            accessTokenVerificationMiddleware,
            body('commission_percentage').notEmpty().withMessage('Please enter a valid Commission percentage').bail()
            .isNumeric().withMessage('Please enter a valid Commission percentage').bail()
            .custom((value) => value >= 0 && value <= 100).withMessage('Please enter a valid Commission percentage'),
            SingleBodyValidationMiddleware,
            commissionPostController
        ]);
        this.app.get('/commissions', [accessTokenVerificationMiddleware, commissionGetController]);
        this.app.put('/commissions', [
            accessTokenVerificationMiddleware,
            body('commission_percentage').notEmpty().withMessage('Please enter a valid Commission percentage').bail()
            .isNumeric().withMessage('Please enter a valid Commission percentage').bail()
            .custom((value) => value >= 0 && value <= 100).withMessage('Please enter a valid Commission percentage'),
            SingleBodyValidationMiddleware,
            body('sellers').notEmpty().withMessage('Please provide valid sellers').bail()
            .isArray().withMessage('Please provide valid sellers in correct format').bail()
            .custom((value: Array < string > ) => value.every(i => (typeof i === "string") && !isNaN(Number(i)) && i !== "")).withMessage('Please provide valid sellers in correct format'),
            SingleBodyValidationMiddleware,
            commissionPutController
        ]);
        return this.app;
    }
}