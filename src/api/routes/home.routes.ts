import express from 'express';
import { RoutesConfig } from '../../config/routes.config';
import { homeController } from '../controllers/home.controller';
import { accessTokenVerificationMiddleware } from '../middlewares/auth.middleware';

export class HomeRoutes extends RoutesConfig{
    constructor(app: express.Application){
        super(app, 'HomeRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/home',[ accessTokenVerificationMiddleware, homeController]);
        return this.app;
    }
};