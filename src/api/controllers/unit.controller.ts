import express from 'express';
import logger from '../../config/logger.config';
import { unitGetService } from '../services/unit.service';

require('dotenv').config();

export const unitGetController = async (req: express.Request, res: express.Response) => {
    const data = await unitGetService();
    if (data.status === 500) {
        logger('error', 'GET', '/units', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({
        "units": data.units
    });
}
