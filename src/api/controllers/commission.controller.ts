import express from 'express';
import logger from '../../config/logger.config';
import {
    sellerIdsExists
} from '../validations/commission.validation';
import {
    commissionPostService,
    commissionGetService,
    commissionPutService
} from '../services/commission.service';
require('dotenv').config();


export const commissionPostController = async (req: express.Request, res: express.Response) => {
    const data = await commissionPostService(req.body.commission_percentage);
    if (data.status === 500) {
        logger('error', 'POST', '/commissions', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const commissionGetController = async (req: express.Request, res: express.Response) => {
    const data = await commissionGetService();
    if (data.status === 500) {
        logger('error', 'GET', '/commissions', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({
        "sellers": data.sellers
    });
}

export const commissionPutController = async (req: express.Request, res: express.Response) => {
    const val_data = await sellerIdsExists(req.body.sellers);
    if (!(val_data.status)) {
        logger('error', 'PUT', '/commissions', val_data.status_code, val_data.log_message);
        return res.status(val_data.status_code).json({
            "message": val_data.message
        });
    }
    const data = await commissionPutService(req.body);
    if (data.status === 500) {
        logger('error', 'PUT', '/commissions', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}