import express from 'express';
import logger from '../../config/logger.config';
import { notificationService } from '../services/notification.service';


export const notificationController = async (req: express.Request, res: express.Response) => {
    const data = await notificationService();
    if(data.status === 500){
        logger('error', 'GET', '/notification', 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json({
        "notifications": data.notification,
    });
}
