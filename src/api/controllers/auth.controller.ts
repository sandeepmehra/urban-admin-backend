import express from 'express';
import { login, refresh } from '../services/auth.service';
require('dotenv').config();


export async function loginController(req: express.Request, res: express.Response) {
    const tokens = await login(req.body);
    if (tokens.access_token === undefined) {
        return res.status(401).json({ "message": process.env.RESPONSE_UNAUTHORIZED });
    }
    return res
        .status(200)
        .setHeader('Authorization', tokens.access_token)
        .json({ "refresh_token": tokens.refresh_token });
}

export async function refreshController(req: express.Request, res: express.Response) {
     await refresh(req, res); 
}