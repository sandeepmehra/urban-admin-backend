import express from 'express';
import logger from '../../config/logger.config';
import { customerPutService, customerGetService } from '../services/customer.service';
require('dotenv').config();


export const customerGetController = async (req: express.Request, res: express.Response) => {
    const data = await customerGetService();
    if (data.status === 500) {
        logger('error', 'GET', '/customer', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
          
    return res.status(data.status).json({
        "customers": data.customers
    });
}

export const customerPutController = async (req: express.Request, res: express.Response) => {
    const data = await customerPutService(req.body);
    if (data.status === 500) {
        logger('error', 'PUT', '/customer', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}