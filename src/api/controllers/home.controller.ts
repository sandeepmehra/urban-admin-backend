import express from 'express';
import logger from '../../config/logger.config';
import { homeService } from '../services/home.service';


export  const homeController = async (req: express.Request, res: express.Response) => {
    const data = await homeService();
    if(data.status === 500){
        logger('error', 'GET', '/home', 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json({
        "verification": data.verification, 
        "inventory": data.inventory, 
        "complaints": data.complaints, 
        // "orders": data.orders, 
        "current_commission": data.current_commission, 
        "notification": data.notification,
    });
}