import express from 'express';
import logger from '../../config/logger.config';
import { complaintPutService, complaintsGetService, complaintResponsePostService } from '../services/complaint.service';
import { getAdminIdByEmail } from '../validations/complaint.validation';
require('dotenv').config();

export const complaintResponsePostController = async (req: express.Request, res: express.Response) =>{
    const val_data = await getAdminIdByEmail(req.body.email);
    if (!(val_data.status) && val_data.data === null) {
        logger('error', 'POST', '/complaint/response', val_data.status_code, val_data.log_message);
        return res.status(val_data.status_code).json({
            "message": val_data.message
        });
    }
    const data = await complaintResponsePostService(req.body, val_data.data);
    if (data.status === 500) {
        logger('error', 'POST', '/complaint/response', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({"response_id": data.id});
}

export const complaintsGetController = async (req: express.Request, res: express.Response) => {
    const data = await complaintsGetService();
    if (data.status === 500) {
        logger('error', 'GET', '/complaints', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({
        "complaints": data.complaints
    });
}

export const complaintPutController = async (req: express.Request, res: express.Response) => {
    const data = await complaintPutService(req.body);
    if (data.status === 500) {
        logger('error', 'PUT', '/complaint/close', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}