import express from 'express';
import logger from '../../config/logger.config';
import { sellerGetService, sellerPutService, sellerApporveRejectPutService } from '../services/seller.service';
require('dotenv').config();

export const sellerGetController = async (req: express.Request, res: express.Response) => {
    const data = await sellerGetService();
    if (data.status === 500) {
        logger('error', 'GET', '/sellers', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({
        "sellers": data.sellers
    });
}

export const sellerApprovePutController = async (req: express.Request, res: express.Response) => {
    const data = await sellerApporveRejectPutService(req.body.id, true);
    if (data.status === 500) {
        logger('error', 'PUT', '/seller/approve', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const sellerRejectPutController = async (req: express.Request, res: express.Response) => {
    const data = await sellerApporveRejectPutService(req.body.id, false);
    if (data.status === 500) {
        logger('error', 'PUT', '/seller/reject', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const sellerPutController = async (req: express.Request, res: express.Response) => {
    const data = await sellerPutService(req.body, Number(req.params.seller_id));
    if (data.status === 500) {
        logger('error', 'PUT', '/seller/:seller_id', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}