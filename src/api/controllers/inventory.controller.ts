import express from 'express';
import logger from '../../config/logger.config';
import { inventoryGetService, inventoryPutService, inventoryApproveRejectPutService } from '../services/inventory.service';
require('dotenv').config();

export const inventoryGetController = async (req: express.Request, res: express.Response) => {
    const data = await inventoryGetService();
    if (data.status === 500) {
        logger('error', 'GET', '/inventory', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({
        "products": data.products
    });
}

export const inventoryApprovePutController = async (req: express.Request, res: express.Response) => {
    const data = await inventoryApproveRejectPutService(req.body.id, true);
    if (data.status === 500) {
        logger('error', 'PUT', '/inventory/approve', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const inventoryRejectPutController = async (req: express.Request, res: express.Response) => {
    const data = await inventoryApproveRejectPutService(req.body.id, false);
    if (data.status === 500) {
        logger('error', 'PUT', '/inventory/reject', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const inventoryPutController = async (req: express.Request, res: express.Response) => {
    const data = await inventoryPutService(req.body, Number(req.params.product_variant_id));
    if (data.status === 500) {
        logger('error', 'PUT', '/inventory/:product_variant_id', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}