import express from 'express';
import logger from '../../config/logger.config';
import { reviewGetService, reviewPutService } from '../services/review.service';
require('dotenv').config();

export const reviewGetController = async (req: express.Request, res: express.Response) => {
    const data = await reviewGetService(Number(req.params.customer_id));
    if (data.status === 500) {
        logger('error', 'GET', '/review', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
          
    return res.status(data.status).json({
        "id": data.review?.id,
        "review": data.review?.review
    });
}

export const reviewPutController = async (req: express.Request, res: express.Response) => {
    const data = await reviewPutService(req.body);
    if (data.status === 500) {
        logger('error', 'PUT', '/commissions', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}