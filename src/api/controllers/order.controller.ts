import express from 'express';
import logger from '../../config/logger.config';
import { orderGetService } from '../services/order.service';


export  const orderGetController = async (req: express.Request, res: express.Response) => {
    const data = await orderGetService();
    if(data.status === 500){
        logger('error', 'GET', '/orders', 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json({
        "orders": data.orders, 
    });
}