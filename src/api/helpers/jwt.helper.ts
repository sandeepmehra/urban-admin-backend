import {
  Response
} from "express";
import jwt from 'jsonwebtoken';
import logger from "../../config/logger.config";
require('dotenv').config();


const jwtAccessSecret: any = process.env.ACCESS_TOKEN_SECRET;
const jwtRefreshTokenSecret: any = process.env.REFRESH_TOKEN_SECRET;

export const generateAccessToken = (name: string, email: string): string =>
  'Bearer ' + jwt.sign({
      name
    },
    jwtAccessSecret, {
      algorithm: 'HS256',
      expiresIn: '2d',
      subject: email,
      audience: process.env.AUDIENCE
    });



export const generateRefreshToken = (name: string, email: string): string =>
  jwt.sign({
      name
    },
    jwtRefreshTokenSecret, {
      algorithm: 'HS256',
      expiresIn: '90d',
      subject: email,
      audience: process.env.AUDIENCE
    });


export const verifyJWT = (token: string, path: string, method: string, token_type: string, res: Response, callback: Function, ignoreExpiration: boolean = false) => {

  const secret = token_type === 'access' ? process.env.ACCESS_TOKEN_SECRET !== undefined ? process.env.ACCESS_TOKEN_SECRET : '' : process.env.REFRESH_TOKEN_SECRET !== undefined ? process.env.REFRESH_TOKEN_SECRET : '';
  jwt.verify(
    token,
    secret, {
      algorithms: ['HS256'],
      audience: process.env.AUDIENCE,
      ignoreExpiration: ignoreExpiration
    },

    function (err, decoded) {
      if (err){
        if (err.message.startsWith(process.env.ERROR_JWT_AUDIENCE !== undefined ? process.env.ERROR_JWT_AUDIENCE : '')) {
          logger('error', method, path, 403, `Invalid audience in ${token_type} token`);
          return res.status(403).json({
            "message": process.env.RESPONSE_FORBIDDEN
          });
        } else if (
          err.message.startsWith(process.env.ERROR_JWT_ALGORITHM !== undefined ? process.env.ERROR_JWT_ALGORITHM : '')
        ) {
          logger('error', method, path, 401, `Invalid algorithm in ${token_type} token`);
          return res.status(401).json({
            "message": process.env.RESPONSE_UNAUTHORIZED
          });
        } else if (
          err.message.startsWith(process.env.ERROR_JWT_SIGNATURE !== undefined ? process.env.ERROR_JWT_SIGNATURE : '')
        ) {
          logger('error', method, path, 401, `Invalid signature in ${token_type} token`);
          return res.status(401).json({
            "message": process.env.RESPONSE_UNAUTHORIZED
          });
        } else if (err.message.startsWith(process.env.ERROR_JWT_EXPIRED !== undefined ? process.env.ERROR_JWT_EXPIRED : '')) {

          logger('error', method, path, 401, `Expired ${token_type} token`);
          return res.status(401).json({
            "message": process.env.RESPONSE_UNAUTHORIZED
          });
      
        } else {
          logger('error', method, path, 401, err.message);
          return res.status(401).json({
            "message": process.env.RESPONSE_UNAUTHORIZED
          });
        }
      }
      else {
        const {name,sub}: any = decoded;
        if (name === undefined) {
            logger('error', method, path, 401, 'missing "name" claim in refresh token');
            return res.status(401).json({
                "message": process.env.RESPONSE_UNAUTHORIZED
            });
        }
        if (sub === undefined) {
            logger('error', method, path,  401, 'missing "sub" claim in refresh token');
            return res.status(401).json({
                "message": process.env.RESPONSE_UNAUTHORIZED
            });
        }
        callback(name, sub);
      }
    }
  )
};