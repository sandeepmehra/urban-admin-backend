import prisma from "../../../prisma/prisma-client";
require('dotenv').config();

export const sellerIdsExists = async (sellers: Array < string > ): Promise < {
    status: boolean,
    status_code: number,
    message: string | undefined,
    log_message: string
} > => {
    try {
        const existsSellers = await prisma.user_seller.count({
            where: {
                "id": {
                    in: sellers.map(Number)
                },
            },
        });
        return {
            status: existsSellers === sellers.length,
            status_code: existsSellers === sellers.length ? 200 : 400,
            message: 'Please provide valid seller ids',
            log_message: 'Provided seller ids not present in database'
        };
    } catch (error: any) {
        return {
            status: false,
            status_code: 500,
            message: process.env.RESPONSE_INTERNAL_SERVER_ERROR,
            log_message: error.message
        };
    }
}