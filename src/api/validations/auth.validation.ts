import validator from 'validator';

const isJWT = (value: string) => value.split('.').length === 3 ? true : false

export const isAuthHeaderValid = (value: string | undefined): boolean =>
    value !== undefined && !validator.isEmpty(value) && ((value: string) => value.split(' ').length == 2 ? true : false) && (value.split(' ')[0] === 'Bearer' ? true : false) && isJWT(value.split(' ')[1]);
export const isRefreshTokenBodyValid = (value: string): boolean => !validator.isEmpty(value) && isJWT(value);