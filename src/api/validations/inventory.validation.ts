import prisma from "../../../prisma/prisma-client";
require('dotenv').config();

export const checkInventoryUnitsExists = async(measurement_unit: string):Promise <boolean> => {
    const existsUnit =  await prisma.product_measurement_unit.findMany({
        where:{
            unit: measurement_unit
        }
    }); 
    if(existsUnit[0].unit !== '' && existsUnit[0].unit !== undefined) return true;
    return false
}


