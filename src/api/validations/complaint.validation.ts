import prisma from "../../../prisma/prisma-client";
require('dotenv').config();

export const getAdminIdByEmail = async (email: string ): Promise < {
    status: boolean,
    status_code: number,
    data: Number| null,
    message: string | undefined,
    log_message: string
} > => {
    try {
        const admin_id = await prisma.user_admin.findUnique({
            where:{
                email: email
            },
            select:{
               id: true
            },
        });
        return {
            status: admin_id !== undefined && admin_id !== null,
            status_code: (admin_id !== undefined && admin_id !== null) ? 200 : 400,
            data: (admin_id !== undefined && admin_id !== null) ? Number(admin_id.id) : null,
            message: 'Please provide valid admin email',
            log_message: 'Provided admin email not present in database'
        };
    } catch (error: any) {
        return {
            status: false,
            status_code: 500,
            data: null,
            message: process.env.RESPONSE_INTERNAL_SERVER_ERROR,
            log_message: error.message
        };
    }
}