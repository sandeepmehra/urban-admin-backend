export interface complaintResponsePostDto {
    complaint_id: string;
    response: string;
    email: string;
}
export interface complaintsResponseDto{
    id: string;
    name: string;
    subject: string;
    description: string;
    status: boolean;
    responses?: responsesArrayDto[] | undefined
}
export interface complaintsDBDto{
    id: string;
    customer: {
        name: string;
    };
    subject: string;
    description: string;
    open: boolean;
    customer_response?: [{
        id: string;
        response: string;
        create_on: string;
    }];
    admin_response?: [{
        id: string;
        response: string;
        created_on: string;
        user_admin: {
            name: string;
        }
    }]
}

export interface combineArrayDto {
    from?: {
        user_admin?:{
            name: string 
        }
    };
    created_on: string;
    response: string
}
export interface responsesArrayDto {
    from?: string;
    datetime: string;
    response: string
}
