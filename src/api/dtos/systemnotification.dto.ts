export interface SystemNotificationDBDto {
    id?: string;
    title:string;
    message?: string;  
    added_on: string;
}

export interface SystemNotificationResponseDto extends Omit<SystemNotificationDBDto, 'added_on'>{
    duration: string;
}