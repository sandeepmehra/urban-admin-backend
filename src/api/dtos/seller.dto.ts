export interface sellerResponseDto{
    id:string;
    seller_name: string;
    email: string;
    mobile_number: string;
    created_at: string;
    commission_percentage: string;
    status: string;
    shop: {
        name: string;
        logo: string;
        description: string;
        url: string;
        wa_number: string;
        mobile_number: string;
        street: string;
        city: string;
        state: string;
        pincode: string;
        numberOfProducts: string;
    },
    verification: {
        aadhar_card: string;
        pan_card: string;
        selfie: string;
        bank_document: string;
        gst_number: string;
        action_taken: string;
        verified: string;
    }
    
}
               
export interface sellerDbDto{
    id: number;
    name: string;
    email: string;
    created_on: string;
    mobile_number: string;
    commission_percentage: string;
    shop: {
        name: string;
        logo: string;
        description: string;
        url: string;
        wa_number: string;
        mobile_number: string;
        address: {
            street: string;
            city: string;
            state: string;
            pincode: string;
        };
        products:  productsDBDto[]
    },
    verification: {
        aadhar_card_url: string;
        pan_card_url: string;
        selfie_url: string;
        bank_document_url: string;
        gst_number: string;
        action_taken: boolean;
        verified: boolean;
    }
}

interface productsDBDto {
    name: string;
}

export interface SellerDto {
    seller_name?: string; 
    email?: string; 
    mobile_number?: number;
}


