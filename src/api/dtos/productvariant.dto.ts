export interface ProductVariantDto {
    id? : BigInt | number;
    price? : number;
    discounted_price? : number;
    quantity? : number;
    color? : string;
    added_on? : string | object;
    verified? : boolean;
    verified_on? : string | object;
    sku? : string;
}

