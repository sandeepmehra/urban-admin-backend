export interface UserSellerDBDto {
    id: number;
    name: string;
    message ? : string;
    email: string;
    mobile_number: string;
    verification: {
        verified: boolean
    } | null;
    shop: object | null;
}

export interface UserSellerResponseDto {
    id: number;
    name: string;
    message ? : string;
    email: string;
    mobileNumber: string;
    location: string | object;
    verified: boolean;
}