export interface BigIntInterface {
    id: BigInt | number;
    [key: string]: any;
}