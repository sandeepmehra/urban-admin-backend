export interface UserCustomerResponseDto {
    id: number;
    name: string;
    message ? : string;
    email: string;
    mobileNumber: string | number;
    suspended: boolean;
    location?: string | object;
    order_items: any | undefined
}


export interface UserCustomerDBDto {
    mobile: any;
    id: number;
    name: string;
    message ? : string;
    email: string;
    mobile_number: string | number;
    customer_order_address: CustomerAddressDto[];
    suspended: boolean;
    orders: OrderItemsDBDto[] | undefined
}
export interface CustomerAddressDto {
    address?: object | null;
}

export interface OrderItemsDBDto{
    order_items: OrderItemDBDto[] | undefined;
    placed_on: Date;
}

export interface UserOrderItemResponseDto {
    order_item_id: string;
    name: string;
    sku: string;
    rating?: number
}
export interface OrderItemDBDto {
    id: number,
    product_variant: {
        sku: string,
        product: {
            name: string,
        }
    },
    order_item_review: {
        rating: number
    }
}