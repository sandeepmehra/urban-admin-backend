export interface orderResponseDto {
    id: string;
    placed_on: string;
    customer: string;
    payment_mode: string;
    status: string,
    total: string;
    address: string;
    order_items: orderItemsResponseDto[]
}
export interface orderItemsResponseDto {
    product_name: string;
    sku: string;
    quantity: string;
    dispatch_date: string;
    delivery_date?: string;
    status: string;
    total: string
}
export interface orderDBDto {
    id: string;
    placed_on: string;
    customer: {
        name: string;
    };
    payment_mode: string;
    total_price: number;
    customer_order_address: {
        name: string;
        address: {
            street: string;
            city: string;
            state: string;
            pincode: number;
        }
    }
    order_items: orderItemsDBDto[]
}
export interface orderItemsDBDto {
    quantity: number;
    price_per_unit: number;
    status: string;
    delivery_date?: string|null;
    dispatch_date: string;
    product_variant: {
        sku: string;
        product: {
            name: string;
        }
    }
}
export interface orderHomeResponseDto {
    id: string;
    status: string|undefined;
}
export interface orderHomeDBDto {
    id: string;
    order_items: {
        status: string|undefined;
    }
}
    
    
      
