export interface CommissionDto{
    id?: BigInt | number;
    commission_percentage: number;
    added_on?: string | object;
}