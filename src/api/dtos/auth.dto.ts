export interface LoginNetworkDto {
    email: string;
    password: string;
}