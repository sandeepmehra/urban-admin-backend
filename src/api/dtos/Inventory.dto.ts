export interface inventoryResponseDto {
    id: string;
    seller_name: string;
    product_name: string;
    action_taken: string;
    price: string;
    quantity: string;
    color: string;
    status: string;
    rating: string;
    category: string;
    measurement: string;
    measurement_unit: string;
    sku: string;
    hsn_code: string;
    country_of_origin: string;
    seo_keywords: string| Array<string> | undefined;
    images: string| Array<string> | undefined;
    videos: string| Array<string> | undefined;
}
export interface inventoryDbDto {
    id: number;
    price: number;
    quantity: number;
    color: string;
    approved: boolean;
    action_taken: boolean;
    measurement: string;
    measurement_unit: string;
    sku: string;
    hsn_code: string;
    country_of_origin: string;
    product: {
        name: string;
        product_category: string;
        shop: {
            name: string;
        }
    };
    product_variant_seo_keywords: {
        seo_keyword: string  
    };
    product_variant_images: {
        url: string;
    };
    product_variant_videos: {
        url: string;
    };
    order_items: {
        order_item_review: {
            rating: number
        }| null;
    } | null;
}

export interface inventoryUpdateDto {
    product?: {
        name?: string;
        product_category?: string;
    }
    product_name?: string;
    category?: string;
    quantity?: number;
    measurement_unit?: string;
    country_of_origin?: string;
    sku?: string;
    hsn_code?: string;
    measurement?: string;
    color?: string;
    seo_keywords?: {
        0?: Array<string>;
        1?: Array<string>;
    };
    images?: Array<string>;
    videos?: Array<string>;
}