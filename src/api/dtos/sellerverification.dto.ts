export interface SellerVerificationDto {
    id?: BigInt | number;
    aadhar_card? : BigInt | number;
    pan_card? : string;
    selfie? : string;
    live_lat? : number;
    live_long? : number;
    added_on : string | object;
    verified : boolean;
    verified_on? : string | object;
}
