import express from 'express';
import {
    validationResult,
    ValidationError
} from 'express-validator';
import logger from '../../config/logger.config';

export const SingleBodyValidationMiddleware = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const error = validationResult(req).formatWith(({
        msg
    }: ValidationError) => msg);

    if (!error.isEmpty()) {
        logger('error', req.method, req.path, 400, error.array()[0]);
        return res.status(400).json({
            'message': error.array()[0]
        });
    }
    next();
}