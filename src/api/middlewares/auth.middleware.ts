import {
    NextFunction,
    Request,
    Response
} from "express";
import logger from "../../config/logger.config";
import {
    verifyJWT
} from "../helpers/jwt.helper";
import {
    isAuthHeaderValid,
} from "../validations/auth.validation";
require('dotenv').config();


export const accessTokenVerificationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if(isAuthHeaderValid(req.headers.authorization))
        verifyJWT(req.headers.authorization !==undefined?req.headers.authorization.split(' ')[1]: '', req.path, req.method, 'access', res, (name: string, sub: string)=>{
            next();
        });
    else {
        logger('error', req.method, req.path, 401, 'Invalid access token');
        return res.status(401).json({"message": process.env.RESPONSE_UNAUTHORIZED});
    }
}