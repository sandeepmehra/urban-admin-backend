import express from "express";
require('dotenv').config();
import * as http from 'http';
import helmet from "helmet";
import cors from 'cors';
import {
    RoutesConfig
} from "./config/routes.config";
import {
    AuthRoutes,
    HomeRoutes,
    CommissionRoutes,
    NotificationRoutes,
    CustomerRoutes,
    ReviewRoutes,
    ComplaintRoutes,
    InventoryRoutes,
    OrderRoutes,
    SellerRoutes,
    UnitRoutes
} from "./api/routes/all.routes";
import logger from "./config/logger.config";


const app: express.Application = express();
const server: http.Server = http.createServer(app);


//helmet for security
app.use(helmet());

// adding middleware to parse all incoming requests as JSON
app.use(express.json());

// adding middleware to allow cross-origin requests
app.use(
    cors({
        credentials: true,
        allowedHeaders: ['Authorization','Origin','X-Requested-With','Content-Type','Accept','X-Access-Token', 'access-control-allow-credentials'],
        exposedHeaders: ['Authorization','Origin','X-Requested-With','Content-Type','Accept','X-Access-Token','access-control-allow-credentials'],
        methods: 'GET,PUT,POST,DELETE,OPTIONS,HEAD,PATCH',
        origin: `${process.env.UI_URL}`,
        preflightContinue: false,
        optionsSuccessStatus: 200,
    })
);

const routes: Array < RoutesConfig > = [
    new AuthRoutes(app), 
    new HomeRoutes(app), 
    new CommissionRoutes(app), 
    new NotificationRoutes(app), 
    new CustomerRoutes(app), 
    new ReviewRoutes(app), 
    new ComplaintRoutes(app), 
    new InventoryRoutes(app),
    new OrderRoutes(app),
    new SellerRoutes(app),
    new UnitRoutes(app)
];

//simple route to make sure our app working properly
const runningMessage = `Server running at http://localhost:${process.env.PORT}`;
app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200).send(runningMessage);
});

const isEnvValid = () => {
    //TODO add all the envs for checking
    const envs = [
        'PORT', 'RESPONSE_UNAUTHORIZED', 'RESPONSE_FORBIDDEN', 'RESPONSE_NOT_FOUND', 'RESPONSE_INTERNAL_SERVER_ERROR', 'ACCESS_TOKEN_SECRET', 'REFRESH_TOKEN_SECRET', 'STAGE', 'DATABASE_URL'
    ];
    envs.forEach((e: string, i: number) => {
        if (process.env[e] === undefined){
            logger('error', 'GET', '/', 500, `Environment Variable: "${e}" is missing`);
            return ;
        }
        if (envs.length === i+1) return runServer();
    });
}

const runServer = () =>{
    server.listen(process.env.PORT, () => {
        routes.forEach((route: RoutesConfig) => {
            logger('info', 'GET', '/', 200, route.getName());
        });
        console.log(runningMessage);
    });
}

isEnvValid();