import { createLogger, format, transports } from "winston";
const { timestamp, printf, combine } = format;

const logLevels = {
  fatal: 0,
  error: 1,
  warn: 2,
  info: 3,
  debug: 4,
  trace: 5,
};

const myFormat = printf(({level, message, timestamp}) =>{
  return ` ${timestamp} - ${level.toUpperCase()} - ${message}`
}); 

const _logger = createLogger({
    levels: logLevels,
    format: combine(timestamp({format: 'YYYY-MM-DD HH:mm:ss'}), myFormat),
    transports: [new transports.Console()],
});

const logger = ((level: string, method:string, path: string, status_code: number, message: string) => {
  _logger.log(level, `${method.toUpperCase()} - ${path} - ${status_code} - ${message}`);
});

export default logger;